# Proyecto-Reign-School Gabriel Iriarte
## Reign Hacker News

Este proyecto es una aplicación FullStack, que consiste en consumir la siguiente [API](https://hn.algolia.com/api/v1/search_by_date?query=nodejs), guardar las noticias en una base de datos, y consumirlas desde el lado del cliente.

## Iniciar el proyecto (Backend)

Primero, hay que crear la variable de entorno (en un archivo .env ) donde se tiene el puerto para la conexión con la base de datos:

```
MONGODB_URI=mongodb://localhost:27017/HackReignNotices
```

A continuación se corre MongoDB en la terminal:

```
mongod
```

Finalmente, se instalan las dependencias y se levanta el servicio:

```
npm install
npm run start:dev
```

## Iniciar el proyecto (Frontend)

Primero, hay que crear la variable de entorno (en un archivo .env ) donde se tiene el puerto para la conexión con el backend:

```
REACT_APP_HOSTBACKEND = http://localhost:3001
```
Se instalan las dependencias y se ejecuta el proyecto:

```
npm install
npm start
```

Se abre http://localhost:3000/ y ya se tiene el proyecto ejecutando.
