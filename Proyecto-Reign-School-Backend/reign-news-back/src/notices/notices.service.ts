import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Notice } from './interfaces/Notice';
import { HttpService } from '@nestjs/axios';
import { CreateNoticeDto } from './dto/create-notice.dto';
import { lastValueFrom } from 'rxjs';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()

export class NoticesService implements OnApplicationBootstrap {
    constructor(@InjectModel('notice') private readonly noticeModel: Model<Notice>,private httpService: HttpService){}

    async createNotice(createNoticeDto: CreateNoticeDto): Promise<Notice> {
        const newNotice = new this.noticeModel(createNoticeDto);
        return await newNotice.save();
    }

    //LLAMADO A LA API CADA UNA HORA
    @Cron(CronExpression.EVERY_HOUR)
    async showEveryHour(){
        await this.createApiHackNews();
       
    }

    async onApplicationBootstrap() {
        await this.createApiHackNews();
    }


    async createApiHackNews(): Promise<string> {
        const resp = await lastValueFrom(this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs'));
        const notices = resp.data.hits;
        

        const noticesFiltered = notices.filter((notice) =>{
            if((notice.story_title || notice.title) && (notice.url || notice.story_url)){
                return notice
            }
        })

        const noticesWithMap = noticesFiltered.map((notice)=>{
            return {
                title: notice.story_title || notice.title,
                author: notice.author,
                url: notice.story_url || notice.url,
                date: notice.created_at
            }
        })


        var hash = {};
        const noticesNoDuplicates = noticesWithMap.filter(function(current) {
        var exists = !hash[current.title];
        hash[current.title] = true;
        return exists;
        });



        noticesNoDuplicates.map( async(notice: CreateNoticeDto)=>{
            const newNotice = new this.noticeModel(notice);
          
            const existNotice = await this.noticeModel.findOne({
                title: notice.title
            })
            if(!existNotice){
                return await newNotice.save();
            }else{
                console.log('Esta noticia ya existe: ', existNotice.title);
            }
        })

        return noticesWithMap;
    }
    
    async deleteNotice(idDeleted: string): Promise<Notice>{
        const deletedNotice = await this.noticeModel.findByIdAndDelete(idDeleted);
        return deletedNotice;
    }

    async getAllNotices() {
        const allNotices = await this.noticeModel.find().exec();
        return allNotices;
    }
}

