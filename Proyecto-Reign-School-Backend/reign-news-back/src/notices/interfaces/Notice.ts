import { Document } from 'mongoose';
export interface Notice extends Document {
  id?: string;
  title: string;
  author: string;
  url: string;
  date: string
}
