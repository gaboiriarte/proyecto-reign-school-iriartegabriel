import { Controller, Get, Delete, Res, Param, Post, Body, HttpStatus } from '@nestjs/common';

import { CreateNoticeDto } from './dto/create-notice.dto';
// import { response } from 'express';
import { NoticesService } from './notices.service';

@Controller('notices')
export class NoticesController {
    constructor(private readonly noticesService: NoticesService){}
    @Get('/') //Decorador: Entrega información extra de un método
    getNotices(): string {
        return 'Esta acción retorna todas las noticias';
    }

    @Delete('/delete/:noticeId')
    async deleteNotice(@Res() response, @Param('noticeId') noticeId: string){
        console.log('se llamo eliminar elemento de la api');
        const noticeDeleted = await this.noticesService.deleteNotice(noticeId);


        return response.status(200).json({
            ok: true,
            noticeDeleted
        });
    }

    @Post('/')
    async createNotice(@Body() createNoticeDto: CreateNoticeDto, @Res() response) {
        const newNotice = await this.noticesService.createNotice(createNoticeDto);
        return response.status(HttpStatus.CREATED).json(newNotice);
    }

    @Get('crearApi')
    async createApiHackNews(@Res() response){
        const noticesInController = await this.noticesService.createApiHackNews();
        return response.status(HttpStatus.OK).json({
            ok: true,
            data: noticesInController
        })
    }

    @Get('obtenerApi')
    async getAllNotices(@Res() response){
        const allNotices = await this.noticesService.getAllNotices();
        console.log('se llamo obtenerApi')
        return response.status(HttpStatus.OK).json({
            ok: true,
            data: allNotices
        })
    }

    
}
