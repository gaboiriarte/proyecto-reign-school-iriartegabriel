import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { NoticesController } from './notices.controller';
import { NoticesService } from './notices.service';
import { MongooseModule } from '@nestjs/mongoose';
import { NoticeSchema }from './schemas/notice.schema';

@Module({
    imports: [MongooseModule.forFeature([ { name  : 'notice', schema: NoticeSchema} ]), HttpModule],
    controllers:  [NoticesController],
    providers:  [NoticesService]
})
export class NoticesModule {}
