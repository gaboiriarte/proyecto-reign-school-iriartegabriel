export class CreateNoticeDto {
    title: string;
    author: string;
    url: string;
    date: string
}
  