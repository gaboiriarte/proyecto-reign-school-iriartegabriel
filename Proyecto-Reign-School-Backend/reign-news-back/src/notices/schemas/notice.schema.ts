import { Schema } from 'mongoose';

export const NoticeSchema = new Schema({
    title: String,
    author: String,
    url: String,
    date: String
});