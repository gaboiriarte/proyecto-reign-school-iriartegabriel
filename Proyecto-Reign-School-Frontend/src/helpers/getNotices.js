import axios from 'axios';

export const getNotices =  async () =>{
    const url = `${process.env.REACT_APP_HOSTBACKEND}/notices/obtenerApi`;

    try {
        const resp = await axios.get(url);
        const data = resp.data;

        return data;
    } catch (error) {
        console.error(error);
        return 'No se logra tener conexión con la base de datos'; 
    }
    
}