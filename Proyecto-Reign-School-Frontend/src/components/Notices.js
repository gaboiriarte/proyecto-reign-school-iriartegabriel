import React from 'react'
import { Notice } from './Notice';

export const Notices = ({arrNews}) => {

    return (
        <div className='notices__container'>
            {
                (arrNews.length!==0) 
                    ?
                    (arrNews.map(arrNew => 
                        (<Notice key={arrNew._id} {...arrNew} />)
                    ))
                    :
                    (<p className='information__paragraph'>No hay noticias disponibles. <i class="fa fa-thin fa-book-open"></i></p>)    
            }
        </div>
    )
}
