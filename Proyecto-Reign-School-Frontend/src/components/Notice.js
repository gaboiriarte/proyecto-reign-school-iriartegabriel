import React from "react";
import moment from "moment";
import "moment/locale/es";
import { useDispatch, useSelector } from "react-redux";
import { uiOpenModal } from "../actions/ui";
import { noticeSetActive } from "../actions/notice";

moment.locale("es");

export const Notice = ({ author, date, title, url, _id }) => {
  
  const noteDate = moment(date);

  const dispatch = useDispatch();
  const {modalOpen} = useSelector(state => state.ui);


  const onClickTrash = () =>{
    dispatch(uiOpenModal());
    dispatch(noticeSetActive({
      _id, title
    }))
  }

  return (
    <>
      <div className="notice__container">
        <a
          href={url}
          className= {`notice__link ` + (url ? '' : 'notice__link--disabled')}
          target='_blank'
          rel="noreferrer"
        >
          <div className="notice__container--left">
            <h3 className="notice__title">{title}</h3>
            <h4 className="notice__author">- {author} -</h4>
          </div>
        </a>
        <div className="notice__container--right">
          <h5 className="notice__hour">{noteDate.format("LLLL")}</h5>
          {
            (!modalOpen) && (<i className="fas fa-trash notice__icon" onClick={onClickTrash}></i>)
          }
        </div>
      </div>
    </>
  );
};
