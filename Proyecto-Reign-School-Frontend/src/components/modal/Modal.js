import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { noticeClearActive, noticeDelete } from '../../actions/notice';
import { uiCloseModal } from '../../actions/ui';

export const Modal = () => {

    const dispatch = useDispatch();
    const {activeNotice} = useSelector(state => state.notice);
    const {title} = activeNotice;
    const confirmDelete = () =>{
        
        dispatch(noticeDelete(activeNotice._id));

    }

    const cancelDelete = () =>{
        dispatch(noticeClearActive());
        dispatch(uiCloseModal());
    }

    return (
        <div className='modal__container'>
            <h1 className='modal__title'>¿Está seguro que desea eliminar esta noticia?</h1>
            <p className='modal__paragraph'>Noticia: "{title}"</p>
            <div className='modal__options'>
                <button className='modal__button modal__button-confirm' onClick={confirmDelete}>CONFIRMAR</button>
                <button className='modal__button modal__button-cancel' onClick={cancelDelete}>CANCELAR</button>
            </div>
        </div>
    )
}
