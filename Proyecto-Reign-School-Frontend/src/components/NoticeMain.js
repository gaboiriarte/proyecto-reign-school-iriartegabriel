import React from 'react'
import { Notices } from './Notices'
import { useFetchNotices } from '../hooks/useFetchNotices';
import { Modal } from './modal/Modal';
import { useSelector } from 'react-redux';


export const NoticeMain = () => {

    const {data, loading} = useFetchNotices();
    const arrNews = data?.data;
    
    const {modalOpen} = useSelector(state => state.ui);
    

    
    return (
        <>
            <div className='header'>
                <h1 className='header__title'>HN Feed</h1>
                <p className='header__paragraph'>We {'<3'} hacker news!</p>
            </div>

            {
                modalOpen && (<Modal/>)
            }
            

            {
                (!loading && arrNews) ? (<Notices arrNews={arrNews}/>) : (<p className='information__paragraph'>{data} <i className="fa-solid fa-comment-slash"></i> </p> )
            }
            
            
        </>
    )
}

