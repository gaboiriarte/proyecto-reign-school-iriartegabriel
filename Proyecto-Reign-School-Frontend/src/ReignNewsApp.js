import React from 'react';
import {Provider} from 'react-redux';

import { NoticeMain } from './components/NoticeMain';
import { store } from './store/store';

export const ReignNewsApp = () => {
    return (
        <Provider store = {store}>
            <NoticeMain/>
        </Provider>
    )
}
