import React from 'react';
import ReactDOM from 'react-dom';
import { ReignNewsApp } from './ReignNewsApp';
import './styles/styles.scss';

ReactDOM.render(
    <ReignNewsApp />,
  document.getElementById('root')
);



