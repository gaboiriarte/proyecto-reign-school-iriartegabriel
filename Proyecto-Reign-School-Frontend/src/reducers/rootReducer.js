import {combineReducers} from 'redux';
import { noticeReducer } from './noticeReducer';
import { uiReducer } from './uiReducer';

export const rootReducer = combineReducers({
    ui: uiReducer,
    notice: noticeReducer
})