import { types } from "../types/types";

const initialState = {
    activeNotice : null
}

export const noticeReducer = (state = initialState, action) =>{

    switch (action.type) {
        case types.noticeSetActive:
            return{
                ...state,
                activeNotice: {
                    ...action.payload
                }
            }
        
        case types.noticeClearActive:
            return{
                ...state,
                activeNotice: null
            }
            
    
        default:
            return state;
    }


}