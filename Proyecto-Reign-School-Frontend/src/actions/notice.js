import axios from "axios";
import { types } from "../types/types";
import { uiCloseModal } from "./ui";


export const noticeSetActive = (notice) =>({
    type: types.noticeSetActive,
    payload: notice
})

export const noticeClearActive = () => ({
    type: types.noticeClearActive
})


export const noticeDelete = (id) => {
    return async(dispatch) => {
        try {
            await axios.delete(`${process.env.REACT_APP_HOSTBACKEND}/notices/delete/${id}`);
            
            dispatch(uiCloseModal());
            window.location.reload();
        } catch (error) {
            console.error(error);
        }
    }
}