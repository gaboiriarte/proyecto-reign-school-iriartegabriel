import { shallow } from 'enzyme';
import { Notices } from '../../components/Notices';

describe('Pruebas en <Notices />', () => {
    const arrNews = [
        {
            title: 'A note for LWN subscribers',
            author: 'jchw',
            url: 'https://lwn.net/Articles/881439/',
            date: '2022-01-20T15:19:13.000Z'
          },
          {
            title: 'Blockchain is not only crappy technology but a bad vision for the future',
            author: 'Quindecillion',
            url: 'https://medium.com/@kaistinchcombe/decentralized-and-trustless-crypto-paradise-is-actually-a-medieval-hellhole-c1ca122efdec',
            date: '2022-01-20T13:43:05.000Z'
          }
    ]

    test('Debe de mostrarse correctamente', () => {
        const wrapper = shallow(<Notices arrNews={arrNews}/>);

        expect(wrapper).toMatchSnapshot();
    });

    test('La cantidad de componentes Notice debe ser igual al largo del arreglo', () => {
        const wrapper = shallow(<Notices arrNews={arrNews}/>);

        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('Notice').length).toBe(arrNews.length);
    });

    test('Si no hay noticias debe de mostrar un mensaje', () => {
        const arrNewsVacio = []
        const wrapper = shallow(<Notices arrNews={arrNewsVacio}/>);


        expect(wrapper.find('p').text().trim()).toBe('No hay noticias disponibles.');

      
    });
    
    
});
