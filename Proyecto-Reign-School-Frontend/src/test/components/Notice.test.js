import React from "react";
import '@testing-library/jest-dom';
import { shallow, mount } from 'enzyme';
import { Notice } from "../../components/Notice";
import {Provider} from 'react-redux';
import { store } from "../../store/store";

jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useLayoutEffect: jest.requireActual('react').useEffect,
}));

describe('Pruebas en <Notice />', () => {

    const notice = {
        author : 'Gabriel Iriarte',
        date : '2022-01-19T14:47:33.000Z',
        title : 'Esto es un título',
        url : 'https://www.youtube.com',
        _id: '12345'
    }

    const wrapper = mount(
    
        <Provider store ={store}>
            <Notice key={notice._id} {...notice}/>
        </Provider>
    )
    // const author = 'Gabriel Iriarte';
    // const date = '2022-01-19T14:47:33.000Z';
    // const title = 'Esto es un título';
    // const url = 'https://www.youtube.com';
    // const _id
    test('Debe verse correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('Debe de tener un h3 y un h4 con el title y el author respectivamente', () => {
        
        expect(wrapper.find('h3').text().trim()).toBe(notice.title);
        expect(wrapper.find('h4').text().trim()).toBe(`- ${notice.author} -`);    

    });
    
    
});
