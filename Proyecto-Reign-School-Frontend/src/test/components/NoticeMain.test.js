import React from "react";
import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import { NoticeMain } from "../../components/NoticeMain";
import { useFetchNotices } from "../../hooks/useFetchNotices";
import {Provider} from 'react-redux';
import { store } from "../../store/store";


jest.mock('../../hooks/useFetchNotices');

describe('Pruebas en <NoticeMain />', () => {
  
    beforeEach(()=>{
        useFetchNotices.mockImplementation(()=>{
            return { data: [], loading: true }
        });
    })

    test('Debe de mostrarse correctamente', () => {
      const wrapper = shallow(
        <Provider store={store}>
          <NoticeMain />
        </Provider>);
      expect(wrapper).toMatchSnapshot();
    });
    
});
