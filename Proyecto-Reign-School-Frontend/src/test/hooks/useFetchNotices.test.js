import { useFetchNotices } from "../../hooks/useFetchNotices";
import {renderHook} from '@testing-library/react-hooks';

describe('Pruebas en el hook use', () => {

  test('Debe de retornar el estado inicial', async() => {
    
    const {result, waitForNextUpdate} = renderHook(() => useFetchNotices());
    const {data,loading} = result.current;

    await waitForNextUpdate({timeout:1500});

    expect(data).toEqual([]);
    expect(loading).toBe(true);
  });

  test('Debe de retornar un arreglo y el loading en false', async() => {
    const {result, waitForNextUpdate} = renderHook(() => useFetchNotices());
    await waitForNextUpdate({timeout:300});
    const {data,loading} = result.current;

    expect(typeof data).toBe('object');
    expect(loading).toBe(false);


  });
  
  
});
