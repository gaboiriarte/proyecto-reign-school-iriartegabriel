import { getNotices } from "../../helpers/getNotices";

describe('Pruebas en getNotices', () => {
  test('Debe traer un array', async() => {
    
    const {data} = await getNotices();
    expect(typeof data).toBe('object');

  });
  
});
