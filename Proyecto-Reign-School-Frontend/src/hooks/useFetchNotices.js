import { useEffect, useState } from 'react';
import { getNotices } from '../helpers/getNotices';

export const useFetchNotices = () =>{
    const [state, setState] = useState({
        data: [],
        loading: true
    });

    useEffect(() => {
        getNotices()
            .then(notices =>{
                setState({
                    data: notices,
                    loading: false
                })
            })
            .catch(e => console.error(e))
        
    }, [])

    return state;
}