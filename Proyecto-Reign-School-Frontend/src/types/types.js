export const types = {
    uiOpenModal : '[ui] Open modal',
    uiCloseModal : '[ui] Close modal',

    noticeSetActive : '[notice] Set active notice',
    noticeDeleted : '[notice] Delete notice',
    noticeClearActive: '[notice] Clear active notice'

}